# README #

Repository of Team 16 Business Process Management WS 16/17 

## Within this Repository 3 Applications are stored. ##
* Synchronous BPEL Order Process
* Asynchronous BPEL Order Process
* Testapplication / Frontend

### Used libraries ###
* OpenUI5 1.42 - JQuery based Frontendlibrary
* jQuery Soap 1.6.10 - Send/Receive SOAP requests in javascript
* jQuery xml2json - parse xml and convert to valid JSON

### How to use the testapplication ###

* Both BPEL process should be deployed using the SimTech Environment
* As most Browsers block HTTP requests across Domain boundaries, please make sure to enable CORS within the browser used.
* Within the app please make sure to provide the correct Port of the running ODE instance as otherwise the requests will fail.

**HINT:**
For Chrome the startparameter "--disable-web-security --user-data-dir" should be used and additionally the Chrome plugin "Allow-Control-Allow-Origin: *" might be necessary depending on your configuration.

### BPMN Processes ###

[Synchronous BPM Process](http://academic.signavio.com/p/model/043cc3ef67bc4145ab1de95094eef2eb/png?inline&authkey=83d95c79658218b6e68f920e07faefe665da50ba46714fd1288b2751d3432)


[Asynchronous BPMN Process](http://academic.signavio.com/p/model/5ae3925d2a424c6b8cf82070be274447/png?inline&authkey=d575049722c22484d60b9b899da5f0147ffdf4b6ad3d36daa88bcca830b335)